#need to install svmutil first
#y is the target, x is data features
from svmutil import *
import pickle

def train():
    angel_x = pickle.load(open("angel_feature.pickle", 'r'))
    emo_x = pickle.load(open("emo_feature.pickle", 'r'))

    print angel_x

    xtrain = angel_x + emo_x
    ytrain = [1]*len(angel_x) + [0]*len(emo_x)

    prob = svm_problem(ytrain,xtrain)               
    param = svm_parameter('-s 0 -t 0 -c 4 -b 1') #-s 3 means epsilon-SVR
    m = svm_train(prob,param)   #m stores the trained matrix
    svm_save_model("svr.model", m)
    return m

def test(xtest):
    m = svm_load_model("svr.model")
    # xtest = pickle.load(open("emo_feature.pickle", 'r'))
    p_label, p_acc, p_val = svm_predict([1]*len(xtest), xtest, m, "-b 1") 
    return [a[0] for a in p_val]
