Train your own model:

load a wavefile and and run:
 
fs, y = wavfile.read('female.wav')
feature, onsets = fvoiced.angel_feature(y, fs)
pickle.dump(feature, open("angel_feature.pickle", 'w'))

then

fs, y = wavfile.read('male.wav')
feature, onsets = fvoiced.angel_feature(y, fs)
pickle.dump(feature, open("demon_feature.pickle", 'w'))

then

regression.train()

Use it as an effect:

run main.py