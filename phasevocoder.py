from scipy import *
from pylab import *
from scipy.io import wavfile
# from scikits.audiolab import wavread
import scipy.signal as signal
import numpy
# import pyaudio
import wave
import sys


#execfile('rec.py')

def func_vd_msf(y):
    m_s_f=[]
    [B,A]=signal.butter(9,.33,btype='low') #butterworth parameter
    y1=signal.lfilter(B, A, y) #lowpass butterworth filter
    m_s_f=sum(abs(y1))
    return m_s_f

def func_vd_zc(y):
    zc=0
    n = arange(0,len(y)-1)
    for index in n:
        #if index>len(y)-1:
        #    break
        zc=zc+(1./2) * abs(sign(y[index+1])-sign(y[index]))
    return zc
        

def func_pitch(y,fs):
    # "calclate the pitch"
    pitch_period=0
    period_min = 40 #round(fs *2e-3)
    period_max = 100 #round(fs * 5e-3)
    R=autocorr(y)
    R_max=max(R)
    R_mid=argmax(R)
    #R_mid=len(R)/2 # hacking here
    #range=arange(R_mid+period_min, R_mid+period_max)
    pitch_per_range = R[R_mid+period_min : R_mid+period_max]
    R_max=max(pitch_per_range)
    R_mid=argmax(pitch_per_range)
    pitch_period = R_mid+period_min
    return pitch_period

def autocorr(x):
    result = correlate(x, x, mode='full')
    return result

def fvoiced(x,fs,fsize):
    #x is the wavfile
    #fs is the sample rate
    #fsize is the trunck size of the frame
    f=1
    b=1
    frame_length = round (fs * fsize)
    N=frame_length-1
    #frame segmentation
    b=arange(0,(len(x)-frame_length),frame_length)
    #empty list declaration
    msf=zeros(len(x))
    zc=zeros(len(x))
    pitch_plot=zeros(len(x))
    voiced=zeros(len(x))
    
    count = 0
    for index in b:
        count=count+1
        y1=x[index:index+N]
        #window=hanning(N)
        y=signal.lfilter([1-0.9378], 1, y1, axis=-1, zi=None)
        #y=y1
        msf[index:index+N]=func_vd_msf(y)
        zc[index:index+N]=func_vd_zc(y)
        pitch_plot[index:index+N]=func_pitch(y,fs)
        
    thresh_msf = (( (sum(msf)/len(msf)) - min(msf)) * 0.67)+min(msf)
    voiced_msf = msf>thresh_msf
    
    thresh_zc = (( (sum(zc)/len(zc)) - min(zc)) * 1.5)+min(zc)
    voiced_zc = zc<thresh_zc
    
    thresh_pitch = (( (sum(pitch_plot)/len(pitch_plot)) - min(pitch_plot)) * 0.5)+min(pitch_plot)
    voiced_pitch = pitch_plot > thresh_pitch
    
    bb=arange(0,len(x)-frame_length)
    for index in bb:
        if (voiced_msf[index]*voiced_pitch[index]*voiced_zc[index] == 1):
            voiced[index]=1
        else:
            voiced[index]=0
    return voiced, pitch_plot

def search(array,key):
    indexList=[]
    index=arange(0,len(array))
    for i in index:
        if array[i]==key:
            indexList.append(i)
    return indexList

def voicedonset(aa):
    candidate=search(aa,1)
    #fist do the smoothing
    onset=[]
    index=arange(1,len(candidate)-1)    
    for i in index:
        if (candidate[i]-candidate[i-1]>1 and candidate[i+1]-candidate[i]==1):
            onset.append(candidate[i])
    return onset

def smooth(aa):
    index=arange(1,len(aa)-1)
    for i in index:
        if aa[i]==0 and aa[i-1]==1 and aa[i+1]==1:
            aa[i]=1
    return aa   



def smooth2(a):
    if a[0]==0 and a[1]>0:
        a[0] = a[1]
    for i in range(len(a)-4):
        if (a[i]-a[i+1])>1 and (a[i+4]-a[i+1])>1:
            a[i+1] = a[i]
        if (a[i+1]-a[i])>1 and (a[i+1]-a[i+4])>1:
            a[i+1] = a[i]
    return a


def getpitch(signalin):
    dsrate = 16
    N = 2048/dsrate
    n1 = 512/dsrate
    win = hanning(N)
    window = np.blackman(35)
    # signalin = np.convolve(np.array(wave.struct.unpack("%dh"%(len(signalin)/1),signalin)), window)
    # sigin = signal.resample(signalin, len(signalin)/dsrate)
    signalin = convolve(array(signalin), window)
    sigin = numpy.array([signalin[i*16] for i in range(len(signalin)/16)])
    L = sigin.size
    a = zeros(N)
    b = zeros(N - numpy.mod(L, n1))
    DAFx_in = numpy.concatenate([a, sigin, b])
    DAFx_in = DAFx_in *1.0/ max(abs(sigin))
    pit_index=[]
    pin = 0
    pend = DAFx_in.size - N
    hs_win = N/2*dsrate
    c = zeros(N*15)
    while pin < pend:
        grain =  DAFx_in[pin:pin+N]*win
        grain = numpy.concatenate([grain, c])
        fc = fft(numpy.fft.fftshift(grain))
        f = fc[0:hs_win]
        r = abs(f)
        pit_index.append(r[37:505].argmax()+37)
        pin += n1
    
    for p in range(100):
	    pit_index = smooth2(pit_index)
    for index in range(len(pit_index)):
        pit_index[index] *= 1.345
    
    return pit_index

def getonsetpitch(pit_index, onsets):
	onsets = [o / 512 for o in onsets]
	onsetpitch = [0]*len(onsets)
	for i in range(len(onsets)-1):
		onsetpitch[i] = mean(pit_index[onsets[i]:onsets[i+1]])
	onsetpitch[len(onsets)-1] = pit_index[onsets[-1]]
	return onsetpitch

def getpos(onset, p):
    for i in range(len(onset)-1):
        if onset[i+1] > p:
            return i
    return len(onset)-1

def getslice(angelness):
	slices = [0]
	for i in range(len(angelness)-1):
		if sign(angelness[i+1])!=sign(angelness[i]):
			 slices.append(i+1)
	return slices

def slice(signalin, onset, angelness, pitches):
	slicedaudio = []
	slicedonset = []
	slicedangelness = []
	slicedpitches = []
	slices = getslice(angelness)
	for i in range(len(onset)):
		onset[i] = int(onset[i])
	for i in range(len(slices)-1):
		sonset = onset[slices[i]]-1
		eonset = onset[slices[i+1]]
		slicedaudio.append(signalin[sonset:eonset])
		onset_n = onset[slices[i]:slices[i+1]]
		onset_n = onset_n - ones(len(onset_n))*onset_n[0]
		slicedonset.append(onset_n)
		slicedangelness.append(angelness[slices[i]:slices[i+1]])
		slicedpitches.append(pitches[slices[i]:slices[i+1]])
	sonset = onset[slices[-1]]-1
	eonset = onset[-1]
	slicedaudio.append(signalin[sonset:eonset])
	onset_n = onset[slices[-1]:len(onset)]
	onset_n = onset_n - ones(len(onset_n))*onset_n[0]
	slicedonset.append(onset_n)
	slicedangelness.append(angelness[slices[-1]:len(angelness)])
	slicedpitches.append(pitches[slices[-1]:len(pitches)])

	return slicedaudio, slicedonset, slicedangelness, slicedpitches


def pitchshiftreverb(signalin, onset, angelness, pit_index, fss):
    targetflag = mean(angelness)
    if targetflag > 0:
        target = [71, 70, 71, 70]
    if targetflag < 0:
        target = [54, 53, 54, 53]
    target = [8.1758 * 1.05946**t for t in target]
    n1 = 512
    N = 2048
    win = hanning(N)
        
    grain = zeros(N)
    hs_win = N/2
    omega = 2*pi*n1*arange(0,hs_win)/N
    phi0 = zeros(hs_win)
    r0 = zeros(hs_win)
    psi = phi0
    res = zeros(n1)
        
    L = signalin.size
    a = zeros(N)
    b = zeros(N - numpy.mod(L, n1))
    DAFx_in = numpy.concatenate([a, signalin, b])
    DAFx_in = DAFx_in *1.0/ max(abs(signalin))
    DAFx_out = []
    
    pin = 0
    pend = DAFx_in.size - N
    T=0.3*44100/n1
    (fs,angelin) = wavfile.read('Rays.wav')
    angelresponse = angelin*1.0/max(abs(angelin))
    (fs,emoin) = wavfile.read('Parking Garage.wav')
    emoresponse = emoin*1.0/max(abs(emoin))
    i = 0
    while pin < pend:
        grain =  DAFx_in[pin:pin+N]*win
        pos = getpos(onset, pin)
        scale = angelness[pos]
        if scale>0:
            pit_ratio = (target[pos%4]+20*targetflag*scale*cos(2*pi/T*i))*1.0/pit_index[pos]
        if scale<0:
            pit_ratio = (target[pos%4]+20*targetflag*rand(1))*1.0/pit_index[pos]
        if scale==0:
            pit_ratio = target[pos%4]*1.0/pit_index[pos]
        # print pit_index[pos], target[pos%4], pit_ratio
        i = i+1
        fc = fft(numpy.fft.fftshift(grain))
        f = fc[0:hs_win]
        r = abs(f)
        phi = angle(f)
        delta_phi = omega + mod(phi-phi0-omega+pi, -2*pi) + pi
        delta_r = (r-r0)/n1
        delta_psi = pit_ratio*delta_phi/n1
        for k in range(n1):
            r0 = r0 + delta_r
            psi = psi + delta_psi
            res[k] = r0.dot(cos(psi))
        phi0 = phi
        r0 = r
        psi = mod(psi+pi, -2*pi)+pi
        DAFx_out = numpy.concatenate([DAFx_out, res])
        pin += n1
    
    DAFx_out = DAFx_out[hs_win+n1:hs_win+n1+L]*1.0/max(abs(DAFx_out))

    if targetflag>0:
        DAFx_outc = numpy.convolve(DAFx_out, angelresponse)
    # if targetflag<0:
    #     DAFx_outc = numpy.convolve(DAFx_out, emoresponse)
    if targetflag == 0:
        DAFx_outc = DAFx_out
    DAFx_outc = DAFx_out
    DAFx_outcc = DAFx_outc*1.0/max(abs(DAFx_outc))

    # wavfile.write('emo_final.wav',fss,array(DAFx_outcc*signalin.max(), dtype = 'int16'))
    return DAFx_outcc

# def playback():
#     chunk = 1024
#     wf = wave.open('emo_final.wav', 'rb')
#     p = pyaudio.PyAudio()
#     stream = p.open(format =
#                 p.get_format_from_width(wf.getsampwidth()),
#                 channels = wf.getnchannels(),
#                 rate = wf.getframerate(),
#                 output = True)
#     data = wf.readframes(chunk)
#     while data != '':
#             stream.write(data)
#             data = wf.readframes(chunk)

#     stream.close()
#     p.terminate()

if __name__ == "__main__":

    y,fs,enc=wavread('s2ofwb.wav')
    voiced,pitch_plot=fvoiced(y, fs, 0.03)
    voiced = smooth(voiced)           #smoothing
    voicedIndex = search(voiced,1)    # find value as 1
    onset = voicedonset(voiced)       # find the onset of every voiced sound
    pit_index=[]
    for index in onset:
        pit_index.append(pitch_plot[index]) #corresponding pitch onset

    # to do
    # angelness from ziwen
    # target list
    angelness = [-0.8]* len(pit_index)
    targetflag = sign(mean(angelness))

    # (fs,signalin) = wavfile.read('s2ofwb.wav')
    signalin,fs,enc=wavread('s2ofwb.wav')
    audio_out = pitchshiftreverb(signalin, targetflag, onset, angelness, pit_index, fs)
    playback()



