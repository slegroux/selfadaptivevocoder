'''
Created on Apr 24, 2013

@author: Toro and Ruofeng
'''
# from scikits.audiolab import wavread, wavwrite
from scipy.io import wavfile
import scipy.signal 
from pylab import *
from numpy import *
import wave
# from mfcc import melScaling

def func_vd_msf(y):
    m_s_f=[]
    [B,A]=scipy.signal.butter(9,.33,btype='low') #butterworth parameter
    y1=scipy.signal.lfilter(B, A, y) #lowpass butterworth filter
    m_s_f=sum(abs(y1))
    return m_s_f

def func_vd_zc(y):
    zc=0
    n = arange(0,len(y)-1)
    for index in n:
        #if index>len(y)-1:
        #    break
        zc=zc+(1./2) * abs(sign(y[index+1])-sign(y[index]))
    return zc
        

def func_pitch(y,fs):
    # "calclate the pitch"
    pitch_period=0
    period_min = 40 #round(fs *2e-3)
    period_max = 70 #round(fs * 5e-3)
    R=autocorr(y)
    R_max=max(R)
    R_mid=argmax(R)
    #R_mid=len(R)/2 # hacking here
    #range=arange(R_mid+period_min, R_mid+period_max)
    pitch_per_range = R[R_mid+period_min : R_mid+period_max]
    R_max=max(pitch_per_range)
    R_mid=argmax(pitch_per_range)
    pitch_period = R_mid+period_min
    return pitch_period

def autocorr(x):
    result = correlate(x, x, mode='full')
    return result

def func_centroid(yraw,fs):
    # initialization
    hop=32
    wlen = 4096
    w=hanning(wlen)
    wlen2=wlen/2
    tx=arange(0,wlen2)
    normW=norm(w,2)
    coef=(wlen/(2*pi))
    pft=0
    window = blackman(35)
    signalin = convolve(array(yraw), window)
    y = [signalin[i*20] for i in range(len(signalin)/20)]
    lf = floor((len(y)-wlen)/hop)+1
    feature_rms=zeros(lf)
    feature_centroid=zeros(lf)
    feature_spread=zeros(lf)
    feature_skewness=zeros(lf)
    pin = 0 
    pend = len(y)-wlen-1
    # start 
    while (pin<pend):
        grain=y[pin:(pin+wlen)] * w
        feature_rms[pft]=norm(grain,2)/normW
        f=scipy.fft(grain, 4096*4)/wlen2
        fx=abs(f[0:wlen2])
        # if pft==200:
        #     plot(fx)
        #     show()
        #     asdfsdfasdf
        feature_centroid[pft]=sum(fx*(tx-1))/sum(fx)
        fx2=(fx-feature_centroid[pft])*(fx-feature_centroid[pft])
        feature_spread[pft]=sum(fx2*(tx-1))/sum(fx2)
        fx3=(fx-feature_centroid[pft])*fx2
        feature_skewness[pft]=sum(fx3*(tx-1))/sum(fx3)
        pft=pft+1
        pin=pin+hop
    return feature_centroid,feature_spread,feature_skewness,feature_rms

def fvoiced(x,fs,fsize):
    #x is the wavfile
    #fs is the sample rate
    #fsize is the trunck size of the frame
    f=1
    b=1
    frame_length = round (fs * fsize) #44100 * 0.03
    N=frame_length-1
    #frame segmentation
    b=arange(0,(len(x)-frame_length),frame_length)
    #empty list declaration
    msf=zeros(len(x))
    zc=zeros(len(x))
    pitch_plot=zeros(len(x))
    voiced=zeros(len(x))
    
    print "finish init"
    count = 0
    for index in b:
        count=count+1
        y1=x[index:index+N]
        #window=hanning(N)
        y=scipy.signal.lfilter([1-0.9378], 1, y1, axis=-1, zi=None)
        #y=y1
        msf[index:index+N]=func_vd_msf(y)
        zc[index:index+N]=func_vd_zc(y)
        pitch_plot[index:index+N]=func_pitch(y,fs)
        
    print " finish pitch calculation, start post processing"      
    thresh_msf = (( (sum(msf)/len(msf)) - min(msf)) * 0.67)+min(msf)
    voiced_msf = msf>thresh_msf
    
    thresh_zc = (( (sum(zc)/len(zc)) - min(zc)) * 1.5)+min(zc)
    voiced_zc = zc<thresh_zc
    
    thresh_pitch = (( (sum(pitch_plot)/len(pitch_plot)) - min(pitch_plot)) * 0.5)+min(pitch_plot)
    voiced_pitch = pitch_plot > thresh_pitch
    
    bb=arange(0,len(x)-frame_length)
    for index in bb:
        if (voiced_msf[index]*voiced_pitch[index]*voiced_zc[index] == 1):
            voiced[index]=1
        else:
            voiced[index]=0
    return voiced, pitch_plot

def search(array,key):
    indexList=[]
    index=arange(0,len(array))
    for i in index:
        if array[i]==key:
            indexList.append(i)
    return indexList

def voicedonset(aa):
    candidate=search(aa,1)
    #fist do the smoothing
    onsetStart=[]
    onsetEnd=[]
    index=arange(1,len(candidate)-1) 
    onsetStart.append(candidate[0])  
    for i in index:
        if (candidate[i]-candidate[i-1]>1 and candidate[i+1]-candidate[i]==1):
            onsetStart.append(candidate[i])
        if (candidate[i]-candidate[i-1]==1 and candidate[i+1]-candidate[i]>1):
            onsetEnd.append(candidate[i])
    onsetEnd.append(candidate[-1]) 
    return onsetStart, onsetEnd

def smooth(aa):
    index=arange(1,len(aa)-1)
    for i in index:
        if aa[i]==0 and aa[i-1]==1 and aa[i+1]==1:
            aa[i]=1
    return aa   

def smooth2(a):
    if a[0]==0 and a[1]>0:
        a[0] = a[1]
    for i in range(len(a)-4):
                if (a[i]-a[i+1])>1 and (a[i+4]-a[i+1])>1:
                        a[i+1] = a[i]
                if (a[i+1]-a[i])>1 and (a[i+1]-a[i+4])>1:
                        a[i+1] = a[i]
    return a
    
def downsample(signalin,ffss):
    dsrate = 16
    N = 2048/dsrate
    n1 = 512/dsrate
    win = hanning(N)
    window = blackman(35)
    signalin = convolve(signalin,window)#(array(wave.struct.unpack("%dh"%(len(signalin)/1),signalin)), window)
    sigin = scipy.signal.resample(signalin, len(signalin)/dsrate)
    L = sigin.size
    a = zeros(N)
    b = zeros(N - mod(L, n1))
    DAFx_in = concatenate([a, sigin, b])
    DAFx_in = DAFx_in *1.0/ max(abs(sigin))
    pit_index=[]
    pin = 0
    pend = DAFx_in.size - N
    hs_win = N/2*dsrate
    c = zeros(N*15)
    while pin < pend:
        grain =  DAFx_in[pin:pin+N]*win
        grain = concatenate([grain, c])
        #fc = scipy.fft(scipy.fft.fftshift(grain))
        fc=scipy.fft(grain)
        f = fc[0:hs_win]
        r = abs(f)
        pit_index.append(r[37:505].argmax()+37)
        pin += n1
    
    pit_index = smooth2(pit_index)
    for index in range(len(pit_index)):
        pit_index[index] *= 1.345
    
    ffss=ffss/dsrate
    return pit_index, ffss

def pitchAverage(onsetStart,onsetEnd,pitch_plot):
    counterIndex=arange(0,len(onsetStart))
    onsetPitch=[]
    for i in counterIndex:
        temp=mean(pitch_plot[onsetStart[i]:onsetEnd[i]])
        temp = round(temp)
        onsetPitch.append(temp)
    return onsetPitch

def majoritySmooth(s, radius=7):
    ss = s
    for i in range(radius, len(s)-radius):
        ss[i] = median(s[i-radius:i+radius])
    return ss

def angel_feature(y, fs):
    voiced,pitch_plot=fvoiced(y,fs,0.03)
    voiced = smooth(voiced)
    onsetStart,onsetEnd=voicedonset(voiced)

    centroid0,spread0,skewness0,rms0=func_centroid(y,fs)

    centroid0 = majoritySmooth(centroid0)
    spread0 = majoritySmooth(spread0)
    skewness0 = majoritySmooth(skewness0)

    centroid = [0]*len(onsetStart)
    spread = [0]*len(onsetStart)
    skewness = [0]*len(onsetStart)
    for i in range(len(onsetStart)):

        if onsetStart[i]/620 < len(centroid0):
            centroid[i] = mean(centroid0[onsetStart[i]/620:onsetEnd[i]/620])
            spread[i] = mean(spread0[onsetStart[i]/620:onsetEnd[i]/620])
            skewness[i] = mean(skewness0[onsetStart[i]/620:onsetEnd[i]/620])
        else:
            centroid[i] = centroid0[-2]
            spread[i] = spread0[-2]
            skewness[i] = skewness0[-2]

    feature = [[a, b, c] for (a, b, c) in zip(centroid, spread, skewness)]

    return feature, onsetStart

if __name__ == "__main__":
    fs, y = wavfile.read('emo.wav')
    voiced,pitch_plot=fvoiced(y,fs,0.03)
    voiced = smooth(voiced)         #smoothing
#    voicedIndex=search(voiced,1)    # find value as 1
    onsetStart,onsetEnd=voicedonset(voiced)       # find the onset of every voiced sound
    pitchOnsetRevised=pitchAverage(onsetStart,onsetEnd,pitch_plot)            

#    for index in onsetStart:
#        pitchOnset.append(pitch_plot[index]) #corresponding pitch onset        
    # normalize the onset 
#    for index in range(len(onset)):
#        onset[index]=onset[index]/float(fs)
#    for index in voicedIndex:
#        voiced[index]=pitch_plot[index]     #pass the value to onset
    print onsetStart
    print onsetEnd
    print pitchOnsetRevised
    centroid,spread,skewness,rms=func_centroid(y,fs)
    # print rms
    # print centroid
    # print spread
    # print skewness
    
    xtrain = [[a, b, c] for (a, b, c) in zip(centroid, spread, skewness)]

    
           
    
        