from scipy.io import wavfile

import fvoiced
import regression
import phasevocoder
import pickle
import numpy

if __name__ == "__main__":
    fs, y = wavfile.read('in.wav')
    feature, onsets = fvoiced.angel_feature(y, fs)

    # pickle.dump(feature, open("angel_feature.pickle", 'w'))

    angelness = regression.test(feature)
    angelness_nosmooth = [a*2-1 for a in angelness]
    angelness = fvoiced.majoritySmooth(angelness_nosmooth, radius=3)

    pit_index = phasevocoder.getpitch(y)
    pitches = phasevocoder.getonsetpitch(pit_index, onsets)

    slices = phasevocoder.getslice(angelness)

    # print slices
    slicedaudio, slicedonset, slicedangelness, slicedpitches = phasevocoder.slice(y, onsets, angelness, pitches)

    # print slicedonset
    # print slicedangelness
    # print slicedpitches

    audio_out = []
    for i in range(len(slicedonset)):
        print i    
        audio_out.extend(phasevocoder.pitchshiftreverb(slicedaudio[i], slicedonset[i], slicedangelness[i], slicedpitches[i], fs))
    # audio_out = phasevocoder.pitchshiftreverb(slicedaudio[0], slicedonset[0], slicedangelness[0], slicedpitches[0], fs)
    audio_out = audio_out / max(audio_out)
    audio_out = numpy.array(audio_out * 2**15, dtype='int16')
    wavfile.write('audio_out.wav', fs, audio_out)